# -*- coding: utf-8 -*-

import json
from odoo import api, http
from odoo.http import request, Response
import base64
import logging
_logger = logging.getLogger(__name__)

import requests

class OAuthZoom(http.Controller):

	@http.route(['/zoom/callback','/zoom/callback/<string:code>'], type = 'http', auth = 'user', website = True)
	def get_callback(self, code, **kw):


		current_uid = request.env.context.get('uid')

		findUser = request.env['omni.zoom.config'].search([('users', '=', current_uid)])
		result = {}
		if findUser:

			for x in findUser:
				# findCompany = request.env['res.company'].search([('id', '=', x.)])
				encodeAppIdSecret = x.users.company_id.client_id +  ':' +  x.users.company_id.client_secret
				stringBytes = encodeAppIdSecret.encode("ascii") 
				base64_bytes = base64.b64encode(stringBytes) 
				base64_string = base64_bytes.decode("ascii") 

				getAccessToken = requests.post(x.users.company_id.access_token_url, 
					data = {
						'grant_type' : 'authorization_code',
						'code' : code,
						'redirect_uri' : str(x.users.company_id.redirect_url)
					},
					headers = {
						'Authorization' : 'Basic ' + str(base64_string)
					})

				# getAccessToken = requests.post(x.users.company_id.access_token_url +  \
				# 	'?grant_type=authorization_code&code=' + code + '&redirect_uri=' + x.users.company_id.redirect_url, \
				# 	headers = {
				# 		'Authorization' : 'Basic ' + str(base64_string)
				# 	})

				getContent = getAccessToken.json()


				# raise Warning('access_token' in getContent)

				if 'access_token' in getContent:

					x.update({
							'zoom_auth_code' : code,
							'zoom_access_token' : getContent['access_token'],
							'zoom_refresh_token' : getContent['refresh_token'],
							'is_authenticated' : True,
							'token_expired_in' :  getContent['expires_in']
						})


		# return Response(json.dumps(getContent), content_type='application/json;charset=utf-8',status=200)
		return request.render('odoo_14_zoom_integration.auth_complete', {})
