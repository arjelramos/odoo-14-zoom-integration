# -- coding: utf-8 --

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp
from odoo.tools.float_utils import float_round
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import requests
import json
import logging
import base64
_logger=logging.getLogger(__name__)


class inheritCalendar(models.Model):
	_inherit = 'calendar.event'


	is_zoom_meeting = fields.Boolean(string = 'Is Zoom Meeting?', default = False)

	preffer_zoom_meeting_pw = fields.Char(string = 'Prefer Zoom Meeting Password')

	meeting_id = fields.Char(string = 'Meeting ID')

	zoom_meeting_link = fields.Char(string = 'Meeting Link')

	last_modified_at = fields.Date(string = 'Last updated at')

	zoom_meeting_pw = fields.Char(string = 'Zoom Password')

	create_instant_meeting = fields.Boolean(string = 'Create Instant Meeting', default = False)

	# other_meeting_url = fields.Char(string = 'Other meeting link')



	@api.model
	def create(self, vals):
		if 'is_zoom_meeting' in vals:
			if vals['is_zoom_meeting'] == True:
				if 'meeting_id' not in vals:
						params = {
							"topic" : vals['name'],
							"type" : 1,
							"password" : vals['preffer_zoom_meeting_pw'],
							"agenda" : vals['description'],
							"settings" : {
								"host_video" : "false",
								"participant_video" :"false",
								"cn_meeting" : "false",
								"in_meeting" : "false",
								"join_before_host" : "false", 
								"mute_upon_entry" : "false",
								"watermark" : "false",
								"use_pmi" : "false",
								"approval_type": 2,
								"audio": "both",
								"auto_recording": "none",
							}
						}
						# raise Warning(params)
						currentUid = self.env.context.get('uid')
						getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
						getJson = self.createMeeting(params)

						if 'code' in getJson:
							getCurrentUser._refreshToken()
							getJson = self.createMeeting(params)

						vals['meeting_id'] = getJson['id']
						vals['zoom_meeting_link'] = getJson['join_url']
						vals['zoom_meeting_pw'] = getJson['password']
						vals['last_modified_at'] = getJson['created_at']

				else:
					if vals['meeting_id'] == False:
						params = {
							"topic" : vals['name'],
							"type" : 1,
							"password" : vals['preffer_zoom_meeting_pw'],
							"agenda" : vals['description'],
							"settings" : {
								"host_video" : "false",
								"participant_video" :"false",
								"cn_meeting" : "false",
								"in_meeting" : "false",
								"join_before_host" : "false", 
								"mute_upon_entry" : "false",
								"watermark" : "false",
								"use_pmi" : "false",
								"approval_type": 2,
								"audio": "both",
								"auto_recording": "none",
							}
						}
						# raise Warning(params)
						currentUid = self.env.context.get('uid')
						getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
						getJson = self.createMeeting(params)

						if 'code' in getJson:
							getCurrentUser._refreshToken()
							getJson = self.createMeeting(params)

						vals['meeting_id'] = getJson['id']
						vals['zoom_meeting_link'] = getJson['join_url']
						vals['zoom_meeting_pw'] = getJson['password']
						vals['last_modified_at'] = getJson['created_at']


		result = super(inheritCalendar, self).create(vals)
		return result

	def openMeeting(self):
		# raise Warning(self.zoom_meeting_link)
		return {
			'name' : 'ZOOM Meeting',
			'res_model' : 'ir.actions.act_url',
			'type' : 'ir.actions.act_url',
			'target' : 'new',
			'url' : self.zoom_meeting_link
		}


	def createMeeting(self, params):
		currentUid = self.env.context.get('uid')
		getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
		createMeeting = []
		if getCurrentUser:
			for x in getCurrentUser:
				if x.is_authenticated == True:
					createMeeting = requests.post(x.users.company_id.base_url + 'users/' + x.zoom_user_id + '/meetings', 
								data = json.dumps(params), 
								headers = {
									'Content-Type' : 'application/json',
									'Authorization' : 'Bearer ' + x.zoom_access_token
								})

				else:
					raise UserError(_('User account is not yet Authenticated.'))
		# raise Warning(createMeeting)
		return createMeeting.json()


	def createMeetingBtn(self):

		if self:
			for rec in self:
				if rec.preffer_zoom_meeting_pw == False:
					raise UserError(_('Zoom Password required'))

				d = datetime.strptime(str(self.start), "%Y-%m-%d %H:%M:%S")
				d.strftime("yyy-MM-dd HH:mm:ss (%Y%m%d %H:%M:%S)")
				duration = round(self.duration)

				if self.is_zoom_meeting == True:
					params = {
							"topic" : str(self.name),
							"type" : 2,
							'start_time' : str(d),
							'duration' : duration * 60,
							"password" : self.preffer_zoom_meeting_pw if self.preffer_zoom_meeting_pw else "123",
							"agenda" : self.description if self.description else "No Description",
							"settings" : {
								"host_video" : "false",
								"participant_video" :"false",
								"cn_meeting" : "false",
								"in_meeting" : "false",
								"join_before_host" : "false", 
								"mute_upon_entry" : "false",
								"watermark" : "false",
								"use_pmi" : "false",
								"approval_type": 2,
								"audio": "both",
								"auto_recording": "none",
							}
						}
					currentUid = self.env.context.get('uid')
					getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
					getJson = self.createMeeting(params)

					if 'code' in getJson:
						getCurrentUser._refreshToken()
						getJson = self.createMeeting(params)
						
					self.update({
						'meeting_id' : getJson['id'],
						'zoom_meeting_link' : getJson['join_url'],
						'zoom_meeting_pw' : getJson['password'],
						'last_modified_at' : getJson['created_at']
					})


	def write(self, vals):
		
		
		if 'is_zoom_meeting' in vals:

			# ZOOM PASSWORD REQUIRED
			if 'preffer_zoom_meeting_pw' in vals:
				if vals['preffer_zoom_meeting_pw'] == False:
					raise UserError(_('Zoom Password required'))
			else:
				if self.preffer_zoom_meeting_pw == False:
					raise UserError(_('Zoom Password required'))

			d = datetime.strptime(str(vals['start'] if 'start' in vals else self.start), "%Y-%m-%d %H:%M:%S")
			d.strftime("yyy-MM-dd HH:mm:ss (%Y%m%d %H:%M:%S)")
			duration = round(vals['duration'] if 'duration' in vals else self.duration)

			if vals['is_zoom_meeting'] == True or self.is_zoom_meeting == True:
				if self.meeting_id != True:
					params = {
						"topic" : str(vals['name'] if 'name' in vals else self.name),
						"type" : 2,
						'start_time' : str(d),
						'duration' : duration * 60,
						"password" : vals['preffer_zoom_meeting_pw'] if 'preffer_zoom_meeting_pw' in vals else \
						self.preffer_zoom_meeting_pw,
						"agenda" : vals['description'] if 'description' in vals else self.description,
						"settings" : {
							"host_video" : "false",
							"participant_video" :"false",
							"cn_meeting" : "false",
							"in_meeting" : "false",
							"join_before_host" : "false", 
							"mute_upon_entry" : "false",
							"watermark" : "false",
							"use_pmi" : "false",
							"approval_type": 2,
							"audio": "both",
							"auto_recording": "none",
						}
					}

					currentUid = self.env.context.get('uid')
					getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
					getJson = self.createMeeting(params)

					if 'code' in getJson:
						getCurrentUser._refreshToken()
						getJson = self.createMeeting(params)

					self.update({
						'meeting_id' : getJson['id'],
						'zoom_meeting_link' : getJson['join_url'],
						'zoom_meeting_pw' : getJson['password'],
						'last_modified_at' : getJson['created_at']
						})

		result = super(inheritCalendar, self).write(vals)

		return result





