# -- coding: utf-8 --

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp
from odoo.tools.float_utils import float_round

import logging
_logger=logging.getLogger(__name__)



class inheritResCompany(models.Model):
	_inherit = "res.company"

	# ZOOM MEETING CREDENTIALS
	client_id = fields.Char(string = "Client ID")
	client_secret = fields.Char(string = "Client Secret")
	auth_url = fields.Char(string = "Authorization URL")
	access_token_url = fields.Char(string = "Access Token URL")
	redirect_url = fields.Char(string = "Redirect URL")
	base_url = fields.Char(string = "Base URL")

	# ZOOM MEETING CONFIGURATIONS

	host_video = fields.Boolean(string = "Host Video", default=False)
	watermark = fields.Boolean(string = "Watermark", default=False)
	participant_video = fields.Boolean(string = "Participant Video", default=False)
	join_before_host = fields.Boolean(string = "Join Before Host", default=False)
	mute_upon_entry = fields.Boolean(string = "Mute Upon Entry", default=False)
	personal_meeting_id = fields.Boolean(string = "Personal Meeting ID", default=False)
	enforce_login = fields.Boolean(string = "Enforce Login", default=False)
	registrant_email_notify = fields.Boolean(string = "Registrant Email Notification", default=False)



	approval_type = fields.Char(string = "Approval Type")
	registration_type = fields.Char(string = "Registration Type")
	audio_field = fields.Char(string = "Audio")
	automatic_recording = fields.Char(string = "Automatic Recording")
	enforce_login_domains = fields.Char(string = "Enforce Login Domains")
	alternative_hosts = fields.Char(string = "Alternative Hosts")
	global_dials = fields.Char(string = "Global Dials")
