import time
import datetime
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import api, fields, models, _
import time
import datetime
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import api, fields, models, _
import requests as req
from odoo.exceptions import UserError, ValidationError
import logging
import base64
_logger=logging.getLogger(__name__)


class zoomUserConfigurations(models.Model):
	_name = "omni.zoom.config"



	zoom_auth_code = fields.Char(string = "Zoom Authentication Code")

	zoom_access_token = fields.Char(string = "Zoom Access Token")

	zoom_refresh_token = fields.Char(string = "Zoom Refresh Token")

	is_authenticated = fields.Boolean(string = "Is Authenticated?", default = False)

	token_expired_in = fields.Char(string = "Token Expires in")

	zoom_user_id = fields.Char(string = "Zoom UserId")

	users = fields.Many2one("res.users", string = "User", default = lambda self: self.env.context.get('uid'), tracking = True)

	name = fields.Char(string = "Name")


	state = fields.Selection([
        ('inactive', 'Inactive'),
        ('active', 'Active'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=3, default='active')


	def _refreshToken(self):
		currentUid = self.env.context.get('uid')

		getCurrentUser = self.env['omni.zoom.config'].search([('users', '=', currentUid)])
		data = False
		getContent = []
		for x in getCurrentUser:
			encodeAppIdSecret = x.users.company_id.client_id +  ':' +  x.users.company_id.client_secret
			stringBytes = encodeAppIdSecret.encode("ascii") 
			base64_bytes = base64.b64encode(stringBytes) 
			base64_string = base64_bytes.decode("ascii") 
			getRequests = req.post(x.users.company_id.access_token_url,
				data = {
					'grant_type' : 'refresh_token',
					'refresh_token' : str(x.zoom_refresh_token)
				},
				headers = {
					'Authorization' : 'Basic ' + str(base64_string)
				})
			getContent = getRequests.json()
			if 'error' in getContent:
					raise UserError(_(getContent['reason']))
			data = x.write({
				'zoom_access_token' : getContent['access_token'],
				'zoom_refresh_token' : getContent['refresh_token'],
				'is_authenticated' : True,
				'token_expired_in' :  getContent['expires_in']
				})


	@api.model
	def create(self, vals):

		checkExistUser = self.env['omni.zoom.config'].search([('users', '=', vals['users'])])

		if checkExistUser:
			raise UserError(_('User already created.'))
		findUser = self.env['res.users'].search([('id', '=', vals['users'])])

		if findUser:
			for x in findUser:
				vals['name'] = x.name
		# raise Warning()
		result = super(zoomUserConfigurations, self).create(vals)
		return result

	def get_authorize(self):
		url = self.users.company_id.auth_url + '?response_type=code&client_id=' \
		 + self.users.company_id.client_id + '&redirect_uri=' + self.users.company_id.redirect_url
		return {
			'name' : 'ZOOM REDIRECT',
			'res_model' : 'ir.actions.act_url',
			'type' : 'ir.actions.act_url',
			'target' : 'new',
			'url' : url
		}


	def get_zoom_userId(self):
		
		getUserId = req.get(self.users.company_id.base_url + 'users/me', 
			headers = { 'Authorization' : 'Bearer ' + self.zoom_access_token})

		getJson = getUserId.json()

		# raise Warning(str(getJson))
		self.update({
			'zoom_user_id' : getJson['id']
			})
		# raise Warning(getUserId.content)
