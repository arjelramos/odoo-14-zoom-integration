# -*- coding: utf-8 -*-
{
    'name': 'OmniTechnical Zoom Integration',
    'version': '0.1',
    'category': 'OmniTechnical Zoom Integrations',
    'description' : '''
    
    ''',
    # 'license': 'AGPL-3',
    'author': 'OmniTechnical Global Solutions',
    'website': 'https://omnitechnical.com',
    'maintainer': 'OmniTechnical Global Solutions',
    'summary': 'Zoom Integration with Odoo',
    'depends': [
        'base',
        'contacts',
        'website',
        'calendar',
        'web',
    ],
    'data': [
        'views/authorize_complete.xml',
        'views/views.xml',
        'views/company.xml',
        'views/zoom_config.xml',
        'views/calendar_view.xml',
        'views/calendar_templates.xml',
        'security/ir.model.access.csv',
    ],
    'qweb' :[
        'static/src/xml/web_calendar.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True
}
